api = 2
core = 7.x

; Contributed modules.

projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3.0-rc4"

projects[allow_all_file_extensions][subdir] = "contrib"
projects[allow_all_file_extensions][version] = "1.1"

projects[apps][subdir] = "contrib"
projects[apps][version] = "1.0-beta7"

projects[bean][subdir] = "contrib"
projects[bean][version] = "1.x-dev"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor][download][type] = "git"
projects[ckeditor][download][url] = "http://git.drupal.org/project/ckeditor.git"
; Use Libraries API for ckeditor
; http://drupal.org/node/1063482#comment-6964504
projects[ckeditor][download][branch] = "7.x-1.x"
projects[ckeditor][download][revision] = "f6abbda"

; Fatal error if Libraries module is enabled after CKEditor
; http://drupal.org/node/1898294#comment-6980796
projects[ckeditor][patch][] = "http://drupal.org/files/ckeditor-install-lib-1898294-2.patch"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0-beta6"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.2"

projects[devel][subdir] = "contrib"
projects[devel][version] = "1.3"

; projects[edit][subdir] = "contrib"
; projects[edit][version] = "1.0-alpha6"
; projects[edit][patch][] = "http://drupal.org/files/edit-eval_toolbar-1906490-10.patch"

projects[entity][subdir] = "contrib"
projects[entity][version] = "1.0"

projects[entityreference][subdir] = "contrib"
projects[entityreference][version] = "1.0"

projects[features][subdir] = "contrib"
projects[features][version] = "2.0-beta1"

projects[fences][subdir] = "contrib"
projects[fences][version] = "1.0"

projects[flexslider][subdir] = "contrib"
projects[flexslider][version] = "2.0-alpha1"

projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.1"

projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-unstable7"

projects[image_resize_filter][subdir] = "contrib"
projects[image_resize_filter][version] = "1.13"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.1"

projects[link][subdir] = "contrib"
projects[link][version] = "1.1"

projects[menu_attributes][subdir] = "contrib"
projects[menu_attributes][version] = 1.0-rc2

projects[menu_block][subdir] = "contrib"
projects[menu_block][version] = "2.3"

projects[media][subdir] = "contrib"
projects[media][download][type] = "get"
projects[media][download][url] = http://ftp.drupal.org/files/projects/media-7.x-2.x-dev.tar.gz

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "1.7"

projects[multiupload_filefield_widget][subdir] = "contrib"
projects[multiupload_filefield_widget][version] = "1.11"

projects[multiupload_imagefield_widget][subdir] = "contrib"
projects[multiupload_imagefield_widget][version] = "1.2"

projects[omega][subdir] = "contrib"
projects[omega][version] = "3.1"

projects[omega_tools][subdir] = "contrib"
projects[omega_tools][version] = "3.0-rc4"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.2"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[superfish][subdir] = "contrib"
projects[superfish][version] = "1.8"

projects[token][subdir] = "contrib"
projects[token][version] = "1.5"

projects[uuid][subdir] = "contrib"
projects[uuid][version] = "1.x-dev"

projects[uuid_features][subdir] = "contrib"
projects[uuid_features][version] = "1.x-dev"
projects[uuid_features][patch][] = "http://drupal.org/files/uuid_features-bean_uuid_export_support-1849668-20.patch"

projects[views][subdir] = "contrib"
projects[views][version] = "3.5"

projects[views_field_view][subdir] = "contrib"
projects[views_field_view][version] = "1.0"

projects[views_field_view][subdir] = "contrib"
projects[views_field_view][download][type] = "git"
projects[views_field_view][download][url] = "http://git.drupal.org/project/views_load_more.git"
projects[views_field_view][download][branch] = "7.x-1.x"
projects[views_field_view][download][revision] = "2f367c5c41e5297050a6fab6aea1864d817028d1"

projects[views_load_more][subdir] = "contrib"
projects[views_load_more][version] = "1.1"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "3.1"

; projects[webform][subdir] = "contrib"
; projects[webform][version] = "3.18"

; projects[webform_reply_to][subdir] = "contrib"
; projects[webform_reply_to][version] = "1.0"

; Libraries

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.0/ckeditor_4.0_standard.tar.gz"
libraries[ckeditor][type] = "libraries"

libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/zipball/master"
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"

libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/archive/1571b1d5d3519246fe5b2e40a14579758afac503.zip"
libraries[superfish][directory_name] = "superfish"
libraries[superfish][type] = "library"
