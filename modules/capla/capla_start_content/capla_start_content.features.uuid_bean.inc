<?php
/**
 * @file
 * capla_start_content.features.uuid_bean.inc
 */

/**
 * Implements hook_uuid_features_default_beans().
 */
function capla_start_content_uuid_features_default_beans() {
  $beans = array();

  $beans[] = array(
    'label' => 'CTA 1',
    'description' => NULL,
    'title' => 'Duis autem vel',
    'type' => 'capla_cta_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'cta-1',
    'view_mode' => 'default',
    'created' => '1365032654',
    'log' => '',
    'uid' => '1',
    'default_revision' => '1',
    'revisions' => array(),
    'uuid' => '282498a1-49fe-4856-8475-044b6a579191',
    //'field_cta_block_image' => array(
    //  'und' => array(
    //    0 => array(
    //      'fid' => '1',
    //      'alt' => NULL,
    //      'title' => NULL,
    //      'width' => '150',
    //      'height' => '150',
    //      'uid' => '1',
    //      'filename' => 'cta-1.png',
    //      'uri' => 'public://cta-1.png',
    //      'filemime' => 'image/png',
    //      'filesize' => '28025',
    //      'status' => '1',
    //      'timestamp' => '1365032602',
    //      'type' => 'image',
    //      'uuid' => 'ece97131-4ee7-4a07-b337-a2342060aedf',
    //      'field_file_image_alt_text' => array(),
    //      'field_file_image_title_text' => array(),
    //      'image_dimensions' => array(
    //        'width' => '350',
    //        'height' => '130',
    //      ),
    //    ),
    //  ),
    //),
    'field_block_body' => array(
      'und' => array(
        0 => array(
          'value' => 'Stet clita kasd gubergren, no sea takimata sanctus est. At vero eos et accusam et justo duo dolores et ea rebum.',
          'format' => NULL,
        ),
      ),
    ),
    'field_cta_block_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://google.com',
          'title' => NULL,
          'attributes' => array(
            'target' => 0,
          ),
        ),
      ),
    ),
  );
  $beans[] = array(
    'label' => 'CTA 2',
    'description' => NULL,
    'title' => 'Duis autem vel eum iriure',
    'type' => 'capla_cta_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'cta-2',
    'view_mode' => 'default',
    'created' => '1365033828',
    'log' => '',
    'uid' => '1',
    'default_revision' => '1',
    'revisions' => array(),
    'uuid' => '5a54fdb4-bf47-4777-8d7b-a3e9bcdffc88',
    //'field_cta_block_image' => array(
    //  'und' => array(
    //    0 => array(
    //      'fid' => '2',
    //      'alt' => NULL,
    //      'title' => NULL,
    //      'width' => '150',
    //      'height' => '150',
    //      'uid' => '1',
    //      'filename' => 'cta-2.png',
    //      'uri' => 'public://cta-2.png',
    //      'filemime' => 'image/png',
    //      'filesize' => '28025',
    //      'status' => '1',
    //      'timestamp' => '1365032602',
    //      'type' => 'image',
    //      'uuid' => 'ece97131-4ee7-4a07-b337-a2342060aedf',
    //      'field_file_image_alt_text' => array(),
    //      'field_file_image_title_text' => array(),
    //      'image_dimensions' => array(
    //        'width' => '150',
    //        'height' => '150',
    //      ),
    //    ),
    //  ),
    //),
    'field_block_body' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto</p>
',
          'format' => 'capla_wysiwyg_advanced',
          'safe_value' => '<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto</p>
',
        ),
      ),
    ),
    'field_cta_block_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.drupal.org',
          'title' => NULL,
          'attributes' => array(
            'target' => 0,
          ),
        ),
      ),
    ),
  );
  $beans[] = array(
    'label' => 'CTA 3',
    'description' => NULL,
    'title' => 'Duis autem vel eum iriure',
    'type' => 'capla_cta_block',
    'data' => array(
      'view_mode' => 'default',
    ),
    'delta' => 'cta-3',
    'view_mode' => 'default',
    'created' => '1365033884',
    'log' => '',
    'uid' => '1',
    'default_revision' => '1',
    'revisions' => array(),
    'uuid' => 'd0f8a55a-cea8-4a64-aefe-e1ac95ab1181',
    //'field_cta_block_image' => array(
    //  'und' => array(
    //    0 => array(
    //      'fid' => '3',
    //      'alt' => NULL,
    //      'title' => NULL,
    //      'width' => '150',
    //      'height' => '150',
    //      'uid' => '1',
    //      'filename' => 'cta-3.png',
    //      'uri' => 'public://cta-3.png',
    //      'filemime' => 'image/png',
    //      'filesize' => '28025',
    //      'status' => '1',
    //      'timestamp' => '1365032602',
    //      'type' => 'image',
    //      'uuid' => 'ece97131-4ee7-4a07-b337-a2342060aedf',
    //      'field_file_image_alt_text' => array(),
    //      'field_file_image_title_text' => array(),
    //      'image_dimensions' => array(
    //        'width' => '150',
    //        'height' => '150',
    //      ),
    //    ),
    //  ),
    //),
    'field_block_body' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl</p>
',
          'format' => 'capla_wysiwyg_advanced',
          'safe_value' => '<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl</p>
',
        ),
      ),
    ),
    'field_cta_block_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://google.com',
          'title' => NULL,
          'attributes' => array(
            'target' => 0,
          ),
        ),
      ),
    ),
  );
  return $beans;
}
