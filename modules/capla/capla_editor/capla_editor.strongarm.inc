<?php
/**
 * @file
 * capla_editor.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function capla_editor_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'context_ui_dialog_enabled';
  $strongarm->value = 1;
  $export['context_ui_dialog_enabled'] = $strongarm;

  return $export;
}
