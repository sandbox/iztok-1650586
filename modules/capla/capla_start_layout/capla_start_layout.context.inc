<?php
/**
 * @file
 * capla_start_layout.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function capla_start_layout_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front';
  $context->description = 'Front page starting layout.';
  $context->tag = 'start';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-cta-1' => array(
          'module' => 'bean',
          'delta' => 'cta-1',
          'region' => 'preface_first',
          'weight' => 0,
        ),
        'bean-cta-2' => array(
          'module' => 'bean',
          'delta' => 'cta-2',
          'region' => 'preface_second',
          'weight' => 0,
        ),
        'bean-cta-3' => array(
          'module' => 'bean',
          'delta' => 'cta-3',
          'region' => 'preface_third',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Front page starting layout.');
  t('start');
  $export['front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = 'Sitewide layout context.';
  $context->tag = 'start';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'superfish-1' => array(
          'module' => 'superfish',
          'delta' => '1',
          'region' => 'menu',
          'weight' => '-10',
        ),
        'menu_block-capla_start_layout-1' => array(
          'module' => 'menu_block',
          'delta' => 'capla_start_layout-1',
          'region' => 'sidebar_second',
          'weight' => '0',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Sitewide layout context.');
  t('start');
  $export['sitewide'] = $context;

  return $export;
}
