<?php
/**
 * @file
 * capla_block.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function capla_block_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
}
