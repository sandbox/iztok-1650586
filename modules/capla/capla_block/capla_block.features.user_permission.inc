<?php
/**
 * @file
 * capla_block.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function capla_block_user_default_permissions() {
  $permissions = array();

  // Exported permission: view any capla_cta_block bean.
  $permissions['view any capla_cta_block bean'] = array(
    'name' => 'view any capla_cta_block bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'bean',
  );

  // Exported permission: view any capla_text_block bean.
  $permissions['view any capla_text_block bean'] = array(
    'name' => 'view any capla_text_block bean',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
