<?php
/**
 * @file
 * capla_block.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function capla_block_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'capla_cta_block';
  $bean_type->label = 'Call to action';
  $bean_type->options = '';
  $bean_type->description = 'Block with a image prefix title and text, all wrapped in a link.';
  $export['capla_cta_block'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'capla_text_block';
  $bean_type->label = 'Text';
  $bean_type->options = '';
  $bean_type->description = 'Simple text block.';
  $export['capla_text_block'] = $bean_type;

  return $export;
}
