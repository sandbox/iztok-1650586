<?php
/**
 * @file
 * capla_filters.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function capla_filters_user_default_permissions() {
  $permissions = array();

  // Exported permission: use text format capla_wysiwyg_advanced.
  $permissions['use text format capla_wysiwyg_advanced'] = array(
    'name' => 'use text format capla_wysiwyg_advanced',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format capla_wysiwyg_basic.
  $permissions['use text format capla_wysiwyg_basic'] = array(
    'name' => 'use text format capla_wysiwyg_basic',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  // Exported permission: use text format capla_wysiwyg_full.
  $permissions['use text format capla_wysiwyg_full'] = array(
    'name' => 'use text format capla_wysiwyg_full',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
