<?php
/**
 * @file
 * capla_filters.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function capla_filters_filter_default_formats() {
  $formats = array();

  // Exported format: WYSIWYG advanced.
  $formats['capla_wysiwyg_advanced'] = array(
    'format' => 'capla_wysiwyg_advanced',
    'name' => 'WYSIWYG advanced',
    'cache' => '1',
    'status' => '1',
    'weight' => '-10',
    'filters' => array(
      'media_filter' => array(
        'weight' => '-46',
        'status' => '1',
        'settings' => array(),
      ),
      'image_resize_filter' => array(
        'weight' => '-45',
        'status' => '1',
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 0,
          ),
        ),
      ),
    ),
  );

  // Exported format: WYSIWYG basic.
  $formats['capla_wysiwyg_basic'] = array(
    'format' => 'capla_wysiwyg_basic',
    'name' => 'WYSIWYG basic',
    'cache' => '1',
    'status' => '1',
    'weight' => '-8',
    'filters' => array(
      'filter_html' => array(
        'weight' => '-10',
        'status' => '1',
        'settings' => array(
          'allowed_html' => '<a> <em> <ul> <ol> <li>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
    ),
  );

  // Exported format: WYSIWYG full.
  $formats['capla_wysiwyg_full'] = array(
    'format' => 'capla_wysiwyg_full',
    'name' => 'WYSIWYG full',
    'cache' => '1',
    'status' => '1',
    'weight' => '-9',
    'filters' => array(
      'filter_autop' => array(
        'weight' => '-48',
        'status' => '1',
        'settings' => array(),
      ),
      'image_resize_filter' => array(
        'weight' => '-47',
        'status' => '1',
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 0,
          ),
        ),
      ),
    ),
  );

  return $formats;
}
