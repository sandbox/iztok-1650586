api = 2
core = 7.x

; Include the definition of how to build Drupal core directly, including patches.
includes[] = "drupal-org-core.make"

; Download the Capla install profile and recursively build all its dependencies.
projects[capla][type] = "profile"
projects[capla][download][type] = "git"
projects[capla][download][branch] = "7.x-1.x"
projects[capla][download][url] = Iztok@git.drupal.org:sandbox/Iztok/1650586.git